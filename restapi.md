# REST API - layanan mandiri


## Get Pasien
Request :
- Method : GET
- Endpoint : `api/v1/pendaftaran/pasien/{NO_RM}` 
- Header : 
    - Accept: `application/json`

Response : 
```json
    {
        "status": 200,
        "message: "success",
        "data": {...}
    }
```

## Get Poli
Request :
- Method : GET
- Endpoint : `/pasien` 
- Header : 
    - Accept: `application/json`

Response : 
```json
    {
        "status": 200,
        "message: "success",
        "data": [...]
    }
```



