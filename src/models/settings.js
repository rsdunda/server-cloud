const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const usersSchema = new Schema({
    kuota: {
        type:Schema.Types.Number,
        required: true
    },
    jam_start: {
        type: Schema.Types.Number,
        required: true
    },
    jam_end: {
        type: Schema.Types.Number,
        required: true
    },
});

module.exports = mongoose.model("Setting", usersSchema);
