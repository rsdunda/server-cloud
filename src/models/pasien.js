const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const pasienSchema = new Schema({
  ID: {
    type: String,
    required: false,
  },
  NIK: {
    type: String,
    required: false,
  },
  NORM: {
    type: String,
    required: false,
  },
  NAMA: {
    type: String,
    required: false,
  },
  GELAR_DEPAN: {
    type: String,
    required: false,
  },
  GELAR_BELAKANG: {
    type: String,
    required: false,
  },
  TEMPAT_LAHIR: {
    type: String,
    required: false,
  },
  TANGGAL_LAHIR: {
    type: String,
    required: false,
  },
  JENIS_KELAMIN: {
    type: String,
    required: false,
  },
  ALAMAT: {
    type: String,
    required: false,
  },
  TANGGAL: {
    type: String,
    required: false,
  },
  STATUS: {
    type: String,
    required: false,
  },
});

module.exports = mongoose.model("Pasien", pasienSchema);
