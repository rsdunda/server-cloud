const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const hariSchema = new Schema({
  _id: Number,
  nama: String,
  id_dokter: Number,
  dokter: String,
});

const jadwalSchema = new Schema({
  id_poli: Number,
  nama_poli: String,
  hari: [hariSchema],
});

module.exports = mongoose.model("Jadwal", jadwalSchema);
