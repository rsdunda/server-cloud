const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const keperluanSchema = new Schema({
    keperluan: {
        type:String,
        required: true
    },
});

module.exports = mongoose.model("Keperluan", keperluanSchema);
