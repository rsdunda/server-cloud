const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const poliSchema = new Schema({
  _id: {
    type: Number,
    required: true,
  },
  nama: {
    type: String,
    required: true,
  },
  kuota: {
    type: Number,
    default: 0,
  },
});

module.exports = mongoose.model("Poli", poliSchema);
