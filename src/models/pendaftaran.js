const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const daftarSchema = new Schema({
  NO_REGIS: {
    type: String,
    required: false,
  },
  TGL_DAFTAR: {
    type: Date,
    required: false,
  },
  TGL_BOOKING: {
    type: Date,
    required: false,
  },
  NORM: {
    type: String,
    required: true,
  },
  NIK: {
    type: String,
    required: false,
  },
  NAMA: {
    type: String,
    required: false,
  },
  TANGGAL_LAHIR: {
    type: String,
    required: false,
  },
  POLI: {
    type: String,
    required: false,
  },
  KEPESERTAAN: {
    type: String,
    required: false,
  },
  KEPERLUAN: {
    type: String,
    required: false,
  },
  JENIS_RUJUKAN: {
    type: String,
    required: false,
  },
  NO_BPJS: {
    type: String,
    required: false,
  },
  NO_RUJUKAN: {
    type: String,
    required: false,
  },
  FOTO_KTP: {
    type: String,
    required: false,
  },
  FOTO_RUJUKAN: {
    type: String,
    required: false,
  },
  FOTO_RUJUKAN_LAMA: {
    type: String,
    required: false,
  },
  FOTO_KONTROL: {
    type: String,
    required: false,
  },
  STATUS_BPJS: {
    type: String,
    required: false,
  },
  NO_SEP: {
    type: String,
    required: false,
  },
  STATUS_PELAYANAN: {
    type: String,
    default: "0",
    required: false,
  },
  STATUS: {
    type: String,
    default: "0",
    required: false,
  },
  PESAN: {
    type: String,
    required: false,
  },
  session: {
    type: String,
    required: false,
  },
});

module.exports = mongoose.model("Pendaftaran", daftarSchema);
