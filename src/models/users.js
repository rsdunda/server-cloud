const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const usersSchema = new Schema({
  nik: String,
  username: String,
  password: String,
  nobpjs: String,
  nama_lengkap: String,
  foto: String,
  status: String,
});

module.exports = mongoose.model("Users", usersSchema);
