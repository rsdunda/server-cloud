const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const dokterSchema = new Schema({
  _id: {
    type: Number,
    required: true,
  },
  nama: {
    type: String,
    required: true,
  },

  smf: {
    type: Number,
    required: true,
  },
});

module.exports = mongoose.model("Dokter", dokterSchema);
