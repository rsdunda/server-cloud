const axios = require("axios");
const cache = require('memory-cache');
const { BASE_URL } = require("./index")
module.exports = {
  getCookie: () => {
    axios.get(`${BASE_URL}/check`).then(data => {
      console.log(((data.headers["set-cookie"][0]).split(" ", 1)[0]).split(";", 1)[0])
      cache.put("cookie", ((data.headers["set-cookie"][0]).split(" ", 1)[0]).split(";", 1)[0])
    })
  },

  setNum: num => {
    return ("000" + num).slice(-4);
  }
  
};
