const sockets = require("socket.io");

const Socket = (() => {
  this.io = null;
  this.configure = (server) => {
    this.io = sockets(server);
  };

  return this;
})();

module.exports = Socket;
