const express = require("express");
const app = express();
const server = require("http").createServer(app);
const bodyParser = require("body-parser");
const serverStatus = require("express-server-status");
const morgan = require("morgan");
const cors = require("cors");
const moment = require("moment");
const consola = require("consola");
const jwt = require("jsonwebtoken");
const mongoose = require("mongoose");
const CronJob = require("cron").CronJob;
const io = require("socket.io")(server, {
  cors: {
    origin: "*",
  },
});

const Token = require("./models/token");

const PORT = process.env.PORT || 80;
// "mongodb://10.128.0.4:27017"
const MONGO_URL = process.env.MONGO_URL || "mongodb://10.128.0.2:27017";
// const MONGO_URL = process.env.MONGO_URL || "mongodb://localhost:27017";
// const MONGO_URL = process.env.MONGO_URL;
app.use(morgan("combined"));
app.use(cors());
app.use(express.static(__dirname + "/public"));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

io.on("connection", (socket) => {
  consola.success("Client Connection", socket.id);
});

const { getCookie } = require("./config/request");

app.use((req, res, next) => {
  req.io = io;
  next();
});

app.get("/gerenate-token-users/:nama/:tokens", (req, res) => {
  const { nama, tokens } = req.params;
  const token = jwt.sign({ token: tokens }, "7qvt6t2738");
  Token.create({
    nama,
    token,
  })
    .then((result) => {
      return res.json({
        code: 200,
        status: "created",
        data: result,
      });
    })
    .catch((error) => {
      return res.status(404).json({
        code: 404,
        status: "Error",
        data: error,
      });
    });
});

app.use("/info", serverStatus(app));
app.use("/api/v1/kamar", require("./router/kamar"));
app.get("/privacy", (req, res) => {
  return res.sendFile(__dirname + "/privacy.html");
});
app.use(async (req, res, next) => {
  try {
    await getCookie();
    const token = req.headers["x-api-key"];
    const checkToken = await Token.findOne({ token });
    if (!checkToken) {
      return res.status(400).json({
        code: 400,
        status: "Bad Request",
        message: "akses anda ditolak",
      });
    }
    next();
  } catch (error) {
    consola.error(error);
  }
});

app.use("/api/v1/pendaftaran", require("./router/pendaftaran"));
app.use("/api/v1/poliklinik", require("./router/poliklinik"));
app.use("/api/v1/users", require("./router/users"));
app.use("/api/v1/kiosk", require("./router/kiosk"));
app.use("/api/v1/bpjs", require("./router/bpjs"));
app.use("/api/v1/darah", require("./router/darah"));
app.use("/api/v1/auth", require("./router/login"));
app.use("/api/v1/dokter", require("./router/dokter"));
app.use("/api/v1/jadwal", require("./router/jadwal"));
app.use("/api/v1/identitas", require("./router/identitas"));
app.use("/api/v1/uploads", require("./router/uploads"));

app.get("*", (req, res) => {
  return res.sendFile(__dirname + "/error.html");
});

mongoose
  .connect(`${MONGO_URL}/rsgost`, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  })
  .then(() => {
    consola.success("Database Connected!!!");
    server.listen(PORT, () => {
      getCookie();
      const job = new CronJob(
        "59 59 * * * *",
        () => {
          consola.success("Hello Cron");
          getCookie();
        },
        null,
        true,
        "Asia/Makassar"
      );
      job.start();

      consola.success(`SERVER RUNNING AT http://localhost:${PORT}`);
    });
  })
  .catch((err) => {
    consola.error("Koneksi Database Error ===>>", err);
  });
