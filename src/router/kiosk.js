const express = require("express");
const axios = require("axios");
const route = express.Router();
const consola = require("consola");
const cache = require("memory-cache");
const { BASE_URL } = require("../config");

// model
const Pendaftaran = require("../models/pendaftaran");

route.get("/norm/:norm", async (req, res, next) => {
  const { norm } = req.params;
  try {
    const pasien = await Pendaftaran.findOne({
      NORM: norm,
      STATUS: 1,
    })
      .sort({ _id: -1 })
      .limit(1);
    if (pasien) {
      return res.status(200).json({
        status: 200,
        success: true,
        message: "Success",
        data: pasien,
      });
    }

    return res.status(200).json({
      status: 200,
      success: false,
      message: "Error",
    });
  } catch (error) {
    consola.error(error);
  }
});

route.get("/scan/:id", async (req, res, next) => {
  const { id } = req.params;
  try {
    const getId = id.split("-")[0];
    const noregis = id.split("-")[2];
    // const check = await Pendaftaran.findOne({ NO_REGIS:noregis })
    // if(check) {

    // }
    req.io.sockets.emit("scan", {
      id: getId,
      noregis: noregis,
    });
    return res.status(200).json({
      status: 200,
      success: true,
      message: "Success",
    });
    // const getId = id.split("-")[0];
  } catch (error) {
    consola.error(error);
  }
});

route.get("/:noregis", async (req, res, next) => {
  const { noregis } = req.params;

  try {
    const pasien = await Pendaftaran.findOne({
      NO_REGIS: noregis,
      STATUS: 1,
    });

    // const config = {
    //   method: "get",
    //   url: `${BASE_URL}bpjs/sep/${pasien.NO_SEP}`,
    //   headers: {
    //     Cookie: cache.get("cookie"),
    //   },
    // };

    // const dataSep = await axios(config);

    if (pasien) {
      return res.status(200).json({
        status: 200,
        success: true,
        message: "Success",
        file: pasien.NO_SEP,
        // data: dataSep.data,
      });
    }

    return res.status(200).json({
      status: 200,
      success: false,
      message: "Error",
    });
  } catch (error) {
    consola.error(error);
  }
});

module.exports = route;
