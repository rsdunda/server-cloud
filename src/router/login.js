const express = require("express");
const route = express.Router();
const consola = require("consola");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const Login = require("../models/login");

route.post("/login", async (req, res, next) => {
  const { username, password } = req.body;
  try {
    const checkUser = await Login.findOne({ username });
    if (!checkUser) {
      return res.status(200).json({
        status: 200,
        message: "Username atau password anda salah",
      });
    }

    const checkPass = await bcrypt.compare(password, checkUser.password);
    if (!checkPass) {
      return res.status(200).json({
        status: 200,
        message: "Username atau password anda salah",
      });
    }

    const data = {
      nama: checkUser.username,
      role: checkUser.role,
    };

    const token = jwt.sign(data, "7qvt6t2738", {
      expiresIn: "1h",
    });

    return res.status(200).json({
      status: 200,
      success: true,
      token: token,
    });
  } catch (error) {
    consola.error(error);
  }
});

route.post("/register", async (req, res, next) => {
  const { username, password, role } = req.body;
  try {
    const checkUsername = await Login.findOne({ username });
    if (checkUsername) {
      return res.status(200).json({
        status: 200,
        message: "Username sudah terdaftar",
      });
    }

    bcrypt.hash(password, 12, (err, passwordHash) => {
      if (err) {
        return res.json({
          status: 500,
          message: "password error",
          error: err,
        });
      } else {
        new Login({
          username,
          password: passwordHash,
          role,
        })
          .save()
          .then(() => {
            return res.status(201).json({
              status: 201,
              success: true,
              message: "User berhasil di daftarkan",
            });
          });
      }
    });
  } catch (error) {
    consola.error(error);
  }
});

module.exports = route;
