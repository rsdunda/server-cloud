const express = require("express");
const axios = require("axios");
const route = express.Router();
const consola = require("consola");
const cache = require("memory-cache");
const { BASE_URL } = require("../config");

route.get("/:nokartu", async (req, res, next) => {
  const { nokartu } = req.params;
  const config = {
    method: "GET",
    url: `${BASE_URL}bpjs/${nokartu}`,
    headers: {
      Cookie: cache.get("cookie"),
    },
  };
  try {
    const bpjs = await axios(config);
    console.log(bpjs.data);
    if (bpjs) {
      return res.status(200).json(bpjs.data);
    }

    return res.status(200).json([]);
  } catch (error) {
    consola.error(error);
  }
});

route.get("/sep/:nosep", async (req, res, next) => {
  const { nosep } = req.params;
  const config = {
    method: "get",
    url: `${BASE_URL}bpjs/sep/${nosep}`,
    headers: {
      Cookie: cache.get("cookie"),
    },
  };
  try {
    const bpjs = await axios(config);
    if (bpjs) {
      return res.status(200).json(bpjs.data);
    }

    return res.status(200).json([]);
  } catch (error) {
    consola.error(error);
  }
});

module.exports = route;
