const express = require("express");
const route = express.Router();
const axios = require("axios");
const { BASE_URL, BASE_URL_DEV } = require("../config");

const Dokter = require("../models/dokter");
// const Poli = require("../models/poli");

route.get("/", async (req, res, next) => {
  try {
    const dokter = await Dokter.find({});
    return res.json(dokter);
  } catch (error) {
    consola.error(error);
  }
});

route.get("/create-dokter", async (req, res, next) => {
  try {
    const dokter = await axios.get(`${BASE_URL}dokter`);
    const filterDokter = dokter.data.map((data) => {
      return {
        _id: data.ID,
        nama: `${data.GELAR_DEPAN}${data.NAMA} ${data.GELAR_BELAKANG}`,
        smf: data.SMF,
      };
    });
    const addDokter = await Dokter.insertMany(filterDokter);
    return res.json(addDokter);
  } catch (error) {
    return res.json(error);
  }
});

// route.post("/", async (req, res, next) => {
//   const { nama, poli } = req.body;
//   try {
//     const addDokter = new Dokter({
//       nama,
//       poli,
//     }).save();

//     if (addDokter) {
//       return res.status(201).json({
//         status: 201,
//         success: true,
//       });
//     }
//   } catch (error) {
//     consola.error(error);
//   }
// });

module.exports = route;
