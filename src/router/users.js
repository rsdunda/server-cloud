const express = require("express");
const route = express.Router();
const consola = require("consola");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

// model
const Users = require("../models/users");

route.get("/:uid", async (req, res, next) => {
  const { uid } = req.params;
  consola.success(uid);
  try {
    const checkUser = await Users.findOne({ session: uid });
    if (checkUser) {
      return res.status(200).json({
        status: 200,
        success: true,
        data: checkUser,
      });
    } else {
      return res.status(200).json({
        status: 200,
        success: false,
      });
    }
  } catch (error) {
    consola.error(error);
  }
});

route.post("/", async (req, res, next) => {
  const { nik, password, nama_lengkap } = req.body;
  const salt = bcrypt.genSaltSync(12);
  try {
    const checkUser = await Users.findOne({ username: nik });
    if (checkUser) {
      return res.json({
        status: 201,
        success: false,
        data: "NIK Sudah terdaftar",
      });
    }

    const hashPass = bcrypt.hashSync(password, salt);

    const addUser = new Users({
      nik,
      username: nik,
      password: hashPass,
      nama_lengkap,
      status: "1",
    });
    await addUser
      .save()
      .then((a) => {
        return res.json({
          status: 201,
          success: true,
          data: "Pendaftaran Berhasil",
        });
      })
      .catch((error) => {
        return res.json({
          status: 201,
          success: false,
          data: "Ada gangguna silahkan hubungi admin",
        });
      });
  } catch (error) {
    return next(error);
  }
});

route.post("/login", async (req, res, next) => {
  const { username, password } = req.body;
  try {
    const checkUser = await Users.findOne({ username });

    if (!checkUser) {
      return res.status(200).json({
        status: 200,
        success: false,
        message: "NIK atau password anda salah",
      });
    }

    const checkPass = await bcrypt.compare(password, checkUser.password);
    if (!checkPass) {
      return res.status(200).json({
        status: 200,
        success: false,
        message: "NIK atau password anda salah",
      });
    }

    const data = {
      id: checkUser._id,
      nik: checkUser.nik,
      nobpjs: checkUser.nobpjs === undefined ? "" : checkUser.nobpjs,
      nama_lengkap: checkUser.nama_lengkap,
      foto: checkUser.foto === undefined ? "" : checkUser.foto,
      status: checkUser.status,
    };

    const token = jwt.sign(data, "7qvt6t2738", {
      expiresIn: "1h",
    });

    return res.status(200).json({
      status: 200,
      success: true,
      token: token,
    });
  } catch (error) {}
});

module.exports = route;
