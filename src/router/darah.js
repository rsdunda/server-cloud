const express = require("express");
const axios = require("axios");
const route = express.Router();
const consola = require("consola");
const cache = require("memory-cache");
const { BASE_URL } = require("../config")

route.get("/", async (req,res,next) => {
    const config = {
        method: "get",
        url: `${BASE_URL}darah`,
        headers: {
          Cookie: cache.get("cookie"),
        },
      };
    try {
        const darah = await axios(config);
        if(darah) {
            return res.status(200).json(darah.data)
        }

        return res.status(200).json([])
    } catch (error) {
        consola.error(error)
    }
})

module.exports = route;
