const express = require("express");
const axios = require("axios");
const { uploadFile } = require("../config/uploads");
const route = express.Router();

route.post("/", uploadFile.single("file"), async (req, res, next) => {
  try {
    return res.status(201).json({
      status: true,
      message: "Success",
      file: req.file.filename,
    });
  } catch (error) {
    return next(error);
  }
});

module.exports = route;
