const express = require("express");
const route = express.Router();
const axios = require("axios");
const { BASE_URL, BASE_URL_DEV } = require("../config");

const Jadwal = require("../models/jadwal");

route.post("/", async (req, res, next) => {
  const { id_poli, nama_poli, hari } = req.body;
  try {
    const checkJadwal = await Jadwal.findOne({ id_poli });
    if (checkJadwal) {
      const checkHari = await Jadwal.find({
        id_poli,
        hari: {
          $elemMatch: {
            _id: hari._id,
          },
        },
      });

      console.log(checkHari.length);
      if (checkHari.length > 0) {
        await Jadwal.updateOne(
          { id_poli, "hari._id": hari._id },
          {
            $set: {
              "hari.$.id_dokter": hari.id_dokter,
              "hari.$.dokter": hari.dokter,
            },
          }
        );
        return res.status(201).json({
          status: 201,
          success: true,
          message: "Success",
        });
      } else {
        await Jadwal.updateOne(
          { id_poli },
          {
            $push: {
              hari: hari,
            },
          }
        );
        return res.status(201).json({
          status: 201,
          success: true,
          message: "Success",
        });
      }

      // await Jadwal.
    }

    const addJadwal = new Jadwal({
      id_poli,
      nama_poli,
      hari: hari,
    }).save();

    if (addJadwal) {
      return res.status(201).json({
        status: 201,
        success: true,
        message: "Success",
      });
    }
    // return res.json(hari);
  } catch (error) {
    console.log(error);
  }
});

route.get("/", async (req, res, next) => {
  try {
    const checkJadwal = await Jadwal.find();
    return res.json(checkJadwal);
  } catch (error) {
    console.log(error);
  }
});

module.exports = route;
