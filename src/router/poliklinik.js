const { default: Axios } = require("axios");
const express = require("express");
const axios = require("axios");
const cache = require("memory-cache");
const route = express.Router();
const { BASE_URL, BASE_URL_DEV } = require("../config");

// model
const Poli = require("../models/poli");
// const Keperluan = require("../models/keperluan");

route.get("/", async (req, res, next) => {
  try {
    const poli = await Poli.find();
    return res.json(poli);
  } catch (error) {
    console.log(error);
  }
});

route.get("/create-poli", async (req, res, next) => {
  try {
    const poli = await axios.get(`${BASE_URL}poli`);
    const filterPoli = poli.data.map((data) => {
      return {
        _id: data.ID,
        nama: data.NAMA,
      };
    });
    const newPoli = await Poli.insertMany(filterPoli);
    return res.json(newPoli);
  } catch (error) {
    return res.json(error)
  }
});

// get Poli
// route.get("/", async (req, res, next) => {
//   try {
//     const checkPoli = await Poli.find();
//     if (checkPoli) {
//       return res.json({
//         status: 200,
//         success: true,
//         data: checkPoli,
//       });
//     }
//   } catch (error) {
//     console.log(error);
//   }
// });

// route.post("/", async (req, res, next) => {
//   const { poli } = req.body;
//   try {
//     console.log(poli);
//     const addPoli = new Poli({
//       nama: poli,
//     });
//     addPoli.save();
//     res.json({
//       status: 200,
//       messages: "Success",
//     });
//   } catch (error) {
//     res.status(500).json({
//       status: 500,
//       message: error,
//     });
//   }
// });

// // get Keperluan
// route.get("/keperluan", async (req, res, next) => {
//   try {
//     const checkPoli = await Keperluan.find();
//     if (checkPoli) {
//       return res.json({
//         status: 200,
//         success: true,
//         data: checkPoli,
//       });
//     }
//   } catch (error) {
//     console.log(error);
//   }
// });

// route.post("/keperluan", async (req, res, next) => {
//   const { keperluan } = req.body;
//   try {
//     const addKeperluan = new Keperluan({
//       keperluan: keperluan,
//     });
//     addKeperluan.save();
//     res.json({
//       status: 200,
//       messages: "Success",
//     });
//   } catch (error) {
//     res.status(500).json({
//       status: 500,
//       message: error,
//     });
//   }
// });

module.exports = route;
