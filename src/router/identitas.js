const express = require("express");
const route = express.Router();
const axios = require("axios");
const { BASE_URL, BASE_URL_DEV } = require("../config");

const Dokter = require("../models/dokter");

route.get("/nik/:nik", async (req, res, next) => {
  const { nik } = req.params;
  try {
    const pasien = await axios.get(`${BASE_URL}pasien/nik/${nik}`);
    return res.json(pasien.data);
  } catch (error) {
    return res.status(500).json({
      status: 500,
      success: false,
      message: error,
    });
  }
});
module.exports = route;
