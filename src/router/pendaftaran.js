const express = require("express");
const axios = require("axios");
const cache = require("memory-cache");
const consola = require("consola");
const moment = require("moment");
const route = express.Router();
const { BASE_URL, BASE_URL_DEV } = require("../config");
const { setNum } = require("../config/request");

// model
const Pasien = require("../models/pasien");
const Pendaftaran = require("../models/pendaftaran");
const Settings = require("../models/settings");
const { uploadFile } = require("../config/uploads");

// get pasien berdasarkan norm
route.get("/norm/:norm", async (req, res, next) => {
  const { norm } = req.params;
  // console.log(cache.get("cookie"));
  const config = {
    method: "GET",
    url: `${BASE_URL}pasien/${norm}`,
    headers: {
      Cookie: cache.get("cookie"),
    },
  };
  try {
    // const checkPendaftar = await Pendaftaran.findOne({
    //   NORM: norm,
    //   TGL_BOOKING: moment().format("YYYY-MM-DD"),
    // });
    // if (checkPendaftar) {
    //   return res.json({
    //     status: 200,
    //     success: false,
    //     message: "Pasien Sudah terdaftar",
    //   });
    // }
    // } else {

    const checkPasien = await Pasien.findOne({ NORM: norm });
    if (checkPasien) {
      return res.json({
        status: 200,
        success: true,
        message: "Success",
        data: checkPasien,
      });
    } else {
      const dataPasien = await axios(config);
      console.log("GET norm from SIMRS");
      console.log(dataPasien.data.data);
      if (dataPasien.data.data) {
        const savePasien = new Pasien(dataPasien.data.data);
        await savePasien.save();

        // return res.json(dataPasien.data.data);
        return res.json({
          status: 200,
          success: true,
          message: "Success",
          data: dataPasien.data.data,
        });
      } else {
        // return res.json({
        //   success: false,
        //   message: "Data tidak di temukan",
        // });
        return res.json({
          status: 200,
          success: false,
          message: "Data tidak di temukan",
          data: null,
        });
      }
    }
    // }
  } catch (error) {
    return res.json({
      status: 200,
      success: false,
      message: "Data tidak di temukan",
      data: null,
    });
  }
});

// menampilkan pasien yang mendaftar
route.get("/", async (req, res, next) => {
  try {
    const pendaftar = await Pendaftaran.find({
      TGL_BOOKING: {
        $gte: moment().format("YYYY-MM-DD"),
      },
      STATUS: "0",
    });
    if (!pendaftar) {
      return res.status(200).json({
        success: false,
        status: 200,
      });
    }

    return res.status(200).json({
      success: true,
      status: 200,
      data: pendaftar,
    });
  } catch (error) {
    consola.error(error);
  }
});

// pencarian info berdasarkan session user
route.get("/:session", async (req, res, next) => {
  const { session } = req.params;
  try {
    const pendaftar = await Pendaftaran.find({
      session: session,
      TGL_BOOKING: {
        $gte: moment().format("YYYY-MM-DD"),
      },
    });
    if (!pendaftar) {
      return res.status(200).json([]);
    }

    return res.status(200).json(pendaftar);
  } catch (error) {
    consola.error(error);
  }
});

route.post(
  "/",
  // uploadFile.fields([
  //   { name: "FOTO_RUJUKAN", maxCount: 1 },
  //   { name: "FOTO_RUJUKAN_LAMA", maxCount: 1 },
  //   { name: "FOTO_KONTROL", maxCount: 1 },
  // ]),
  async (req, res, next) => {
    const {
      TGL_BOOKING,
      NORM,
      NIK,
      NAMA,
      TANGGAL_LAHIR,
      POLI,
      KEPESERTAAN,
      KEPERLUAN,
      JENIS_RUJUKAN,
      NO_BPJS,
      NO_RUJUKAN,
      FOTO_KTP,
      FOTO_RUJUKAN,
      FOTO_RUJUKAN_LAMA,
      FOTO_KONTROL,
      STATUS_BPJS,
      NO_SEP,
      STATUS_PELAYANAN,
      STATUS,
      session,
    } = req.body;

    // consola.success(req.io);

    try {
      const kuota = await Settings.findOne({});
      const tgl_sekarang = moment().format("DD-MM-YYYY");
      const tgl_book = moment(TGL_BOOKING).format("DD-MM-YYYY");
      const checkStatus = await Pendaftaran.find({
        $and: [{ NORM }, { TGL_BOOKING }, { STATUS: "0" }],
      })
        .sort({ _id: -1 })
        .limit(1);

      // 1. check tanggal booking tgl_sekaran > tgl_booking = true

      if (checkStatus.STATUS == "0" || checkStatus.STATUS == "1") {
        if (checkStatus.TGL_BOOKING >= tgl_book) {
          return res.status(200).json({
            success: false,
            message:
              "Anda sudah mendaftar pada tanggal booking tersebut, jika ingin mendaftar silahkan batalkan pendaftaran sebelumnya",
            status: 200,
          });
        }
      }

      if (
        tgl_book == tgl_sekarang ||
        parseInt(moment(TGL_BOOKING).format("DD")) < moment().format("DD")
      ) {
        if (parseInt(moment().format("HH")) >= 10) {
          return res.status(200).json({
            success: false,
            message:
              "Waktu pendaftaran pada tanggal kunjungan yang Anda tentukan sudah di tutup. silahkan mendaftar pada tanggal kunjungan berikutnya!",
            status: 200,
          });
        }
      }

      // 2. check kuota poli
      const checkPoli = await Pendaftaran.find({
        POLI: POLI,
        TGL_BOOKING: TGL_BOOKING,
      });
      if (checkPoli.length > 20) {
        return res.status(200).json({
          success: false,
          message: `Pendaftaran pada tanggal kunjungan dan Poliklinik ${POLI} yang Anda tentukan sudah penuh. silahkan pilih tanggal pendaftaran berikutnya`,
          status: 200,
        });
      }

      // return res.status(201).json({
      //   success: false,
      //   message: 2,
      //   status: 201,
      //   data: kuota.kuota,
      // });

      let noregis = "";
      let tanggal = moment().format("DDMMYYYYhhmms");
      const checkNoReg = await Pendaftaran.findOne().sort({ _id: -1 }).limit(1);
      if (!checkNoReg) {
        noregis = `0001${tanggal}`;
      } else {
        let antrian = checkNoReg.NO_REGIS.substr(0, 4);
        let no = parseInt(antrian) + 1;
        noregis = `${setNum(no)}${tanggal}`;
      }

      const newDate = await moment().format("DDMMYYYY");
      const checkPendaftar = await Pendaftaran.findOne({ NORM: NORM })
        .sort({ _id: -1 })
        .limit(1);
      if (checkPendaftar) {
        const oldDate = await moment(checkPendaftar.TGL_BOOKING).format(
          "DDMMYYYY"
        );
        if (oldDate == newDate) {
          return res.status(200).json({
            success: false,
            message: 2,
            status: 200,
          });
        }
      }

      const daftarkan = new Pendaftaran({
        NO_REGIS: noregis,
        TGL_DAFTAR: moment().format("YYYY-MM-DD"),
        TGL_BOOKING: moment(TGL_BOOKING).format("YYYY-MM-DD hh:mm:ss"),
        NORM: NORM,
        NIK: NIK,
        NAMA: NAMA,
        TANGGAL_LAHIR: TANGGAL_LAHIR,
        POLI: POLI,
        KEPESERTAAN: KEPESERTAAN,
        KEPERLUAN: KEPERLUAN,
        JENIS_RUJUKAN: JENIS_RUJUKAN,
        NO_BPJS: NO_BPJS,
        NO_RUJUKAN: NO_RUJUKAN,
        FOTO_KTP: FOTO_KTP,
        FOTO_RUJUKAN: FOTO_RUJUKAN,
        FOTO_RUJUKAN_LAMA: FOTO_RUJUKAN_LAMA,
        FOTO_KONTROL: FOTO_KONTROL,
        STATUS_BPJS: STATUS_BPJS,
        NO_SEP: NO_SEP,
        STATUS_PELAYANAN: STATUS_PELAYANAN,
        STATUS: STATUS,
        PESAN: "",
        session: session,
      });
      // ...data, TGL_BOOKING: moment(data.TGL_BOOKING).format("DD/MM/YYYY"),
      await daftarkan
        .save()
        .then((data) => {
          req.io.sockets.emit("pendaftaran", "Pendaftaran");
          return res.status(201).json({
            success: true,
            status: 201,
            message: 0,
            data: {
              _id: data._id,
              NO_REGIS: data.NO_REGIS,
              TGL_DAFTAR: data.TGL_DAFTAR,
              TGL_BOOKING: moment(data.TGL_BOOKING).format("DD/MM/YYYY"),
              NORM: data.NORM,
              NIK: data.NIK,
              TANGGAL_LAHIR: data.TANGGAL_LAHIR,
              POLI: data.POLI,
              KEPESERTAAN: data.KEPESERTAAN,
              KEPERLUAN: data.KEPERLUAN,
              JENIS_RUJUKAN: data.JENIS_RUJUKAN,
              NO_BPJS: data.NO_BPJS,
              NO_RUJUKAN: data.NO_RUJUKAN,
              FOTO_KTP: data.FOTO_KTP,
              FOTO_RUJUKAN: data.FOTO_RUJUKAN,
              FOTO_KONTROL: data.FOTO_KONTROL,
              STATUS_BPJS: data.STATUS_BPJS,
              NO_SEP: data.NO_SEP,
              STATUS_PELAYANAN: data.STATUS_PELAYANAN,
              STATUS: data.STATUS,
              PESAN: data.PESAN,
              session: data.session,
            },
          });
        })
        .catch((err) => {
          return res.status(201).json({
            success: false,
            message: 1,
            status: 201,
          });
        });
    } catch (error) {
      consola.error(error);
    }
  }
);

route.put("/", async (req, res, next) => {
  const { NO_REGIS, STATUS, NO_SEP } = req.body;
  try {
    const checkNoReg = await Pendaftaran.updateOne(
      { NO_REGIS: NO_REGIS },
      {
        $set: {
          STATUS: STATUS,
          NO_SEP: NO_SEP,
        },
      }
    );
    req.io.sockets.emit("layanan", "layanan");
    if (!checkNoReg) {
      return res.status(201).json({
        status: 201,
        success: false,
        message: "Pendaftaran Gagal",
      });
    }

    return res.status(201).json({
      status: 201,
      success: true,
      message: "Pendaftaran Success!",
    });
  } catch (error) {
    consola.error(error);
  }
});

route.put("/layani/:noregis", async (req, res, next) => {
  const { noregis } = req.params;
  const { status } = req.body;
  try {
    const layanani = await Pendaftaran.updateOne(
      {
        NO_REGIS: noregis,
      },
      {
        $set: {
          STATUS_PELAYANAN: status,
        },
      }
    );
    req.io.sockets.emit("layanan", "layanan");
    if (!layanani) {
      return res.status(201).json({
        status: 201,
        success: false,
      });
    }

    return res.status(201).json({
      status: 201,
      success: true,
    });
  } catch (error) {
    consola.error(error);
  }
});

route.put("/tolak/:noregis", async (req, res, next) => {
  const { noregis } = req.params;
  const { pesan } = req.body;
  try {
    const layanani = await Pendaftaran.updateOne(
      {
        NO_REGIS: noregis,
      },
      {
        $set: {
          STATUS: "2",
          PESAN: pesan,
        },
      }
    );
    req.io.sockets.emit("layanan", "layanan");
    if (!layanani) {
      return res.status(201).json({
        status: 201,
        success: false,
      });
    }

    return res.status(201).json({
      status: 201,
      success: true,
    });
  } catch (error) {
    consola.error(error);
  }
});

module.exports = route;
